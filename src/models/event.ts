import { Document, model, Schema } from '@kominal/lib-node-mongodb-interface';

export interface Event {
	_id?: string;
	tenantId: string;
	projectName: string;
	environmentName: string;
	serviceName: string;
	taskId: string | undefined;
	time: Date;
	type: string;
	content: any;
}

export const EventDatabase = model<Document & Event>(
	'Event',
	new Schema(
		{
			tenantId: { type: Schema.Types.ObjectId, ref: 'Tenant' },
			projectName: String,
			environmentName: String,
			serviceName: String,
			taskId: String,
			time: Date,
			type: String,
			content: Schema.Types.Mixed,
		},
		{ minimize: false }
	)
		.index({ time: 1 }, { expireAfterSeconds: 120960 })
		.index({ time: 1, tenantId: 1, projectName: 1, environmentName: 1, type: 1, serviceName: 1, taskId: 1 })
);
